﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Astar1
{
    public class FindPath : MonoBehaviour {
    	Grid grid;

    	// Use this for initialization
    	void Start () {
    		grid = GetComponent<Grid> ();
    	}

    	// 生成路径
    	void GeneratePath(Node startNode, Node endNode) {
    		List<Node> path = new List<Node>();
    		if (endNode != null) {
    			Node temp = endNode;
    			while (temp != startNode) {
    				path.Add (temp);
    				temp = temp.parent;
    			}
    			// 反转路径
    			path.Reverse ();
    		}
    		// 更新路径
    //        grid.updatePath(LineOfSight(path));
            grid.UpdatePath(path);
    	}

//        List<NodeItem> LineOfSight(List<NodeItem> path)
//        {
//            List<NodeItem> list = new List<NodeItem>();
//            list.Add(path[0]);
//
//            NodeItem startNode = path[0];
//            NodeItem nextNode;
//
//            int checkIndex = 1;
//
//            while( checkIndex < path.Count )
//            {
//                nextNode = path[checkIndex];
//               
//                if( !Physics.Linecast( startNode.pos, nextNode.pos ) )
//                {
//                    checkIndex++;
//                }
//                else
//                {
//                    if( checkIndex == path.Count - 1 )
//                    {
//                        list.Add(path[checkIndex - 1] );
//                        list.Add(path[checkIndex] );
//                    }
//                    else
//                    {
//                        list.Add(path[checkIndex - 1] );
//                        startNode = path[checkIndex - 1];
//                    }
//
//                    checkIndex++;
//                }
//            }
//
//            return list;
//        }

        //曼哈顿估价法
        int Manhattan(Node a, Node b)
        {
            return Mathf.Abs(a.X - b.X) * 10 + Mathf.Abs(a.Y + b.Y) * 10;
        }

        //对角线估价法
        int Diagonal(Node a, Node b)
        {
            int dx = Mathf.Abs(a.X - b.X);
            int dy = Mathf.Abs(a.Y - b.Y);
            int diag = Mathf.Min(dx, dy);
            int straight = dx + dy;

            return 14 * diag + 10 * (straight - 2 * diag);
        }

        // A*寻路
        public void FindingPath(Vector2 start, Vector2 end) {
            System.Diagnostics.Stopwatch sw = new System.Diagnostics.Stopwatch();
            sw.Start();

            Node startNode = grid.GetNode (start);
            Node endNode = grid.GetNode (end);

            Heap<Node> openSet = new Heap<Node>(grid.Max);
            HashSet<Node> closeSet = new HashSet<Node> ();
            openSet.Add (startNode);

            while (openSet.Count > 0) {
                Node curNode = openSet.RemoveFirst();
                closeSet.Add (curNode);

                // 找到的目标节点
                if (curNode == endNode) {
                    sw.Stop();
                    Debug.Log("found: " + sw.ElapsedMilliseconds + " ms");

                    GeneratePath (startNode, endNode);
                    return;
                }

                // 判断周围节点，选择一个最优的节点
                foreach (var item in grid.GetNeibourhood(curNode)) {
                    // 如果是墙或者已经在关闭列表中
                    if (item.isWall || closeSet.Contains (item))
                        continue;
                    // 计算当前相领节点现开始节点距离
                    int newCost = curNode.gCost + Diagonal (curNode, item);
                    // 如果距离更小，或者原来不在开始列表中
                    if (newCost < item.gCost || !openSet.Contains (item)) {
                        // 更新与开始节点的距离
                        item.gCost = newCost;
                        // 更新与终点的距离
                        item.hCost = Manhattan (item, endNode);
                        // 更新父节点为当前选定的节点
                        item.parent = curNode;
                        // 如果节点是新加入的，将它加入打开列表中
                        if (!openSet.Contains (item)) {
                            openSet.Add (item);
                        }
                    }
                }
            }

            GeneratePath (startNode, null);
        }
    }
}
