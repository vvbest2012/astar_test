﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

namespace Astar1
{
    public class Grid : MonoBehaviour 
    {
        [SerializeField]
        RectTransform pathLineRoot;
        [SerializeField]
        Image pathLine;

        int rows;
        int columns;
        int size;

        Node[,] NodeArray;
        List<RectTransform> pathNodeList = new List<RectTransform> ();

        public Texture2D t2d;
        public Transform grid;
        public Image framePre;

        public int Max { get { return rows * columns; } }

        public void Init (int rows, int columns, int size)
        {
            this.rows = rows;
            this.columns = columns;
            this.size = size;

            NodeArray = new Node[rows, columns];

            for (int x = 0; x < rows; x++)
            {
                for (int y = 0; y < columns; y++)
                {
                    var node = new Node(x, y, size);
                    NodeArray[x, y] = node;

//                    var frame = Instantiate(framePre);
//                    frame.transform.SetParent(grid, false);
//                    frame.rectTransform.sizeDelta = Vector2.one * (size - 2);
//                    frame.rectTransform.anchoredPosition = node.Position;

                    if (t2d.GetPixel((int)node.Position.x, (int)node.Position.y) == Color.black)
                    {
                        node.isWall = true;

                 
//                        frame.color = Color.red;
                    }
                        
                }
            }

//            Debug.Log(t2d.GetPixel(100, 100));

        }

    	public Node GetNode (Vector2 coordinate) 
        {
            return NodeArray [(int)coordinate.x, (int)coordinate.y];
    	}

    	public List<Node> GetNeibourhood (Node node)
        {
    		List<Node> list = new List<Node> ();

    		for (int i = -1; i <= 1; i++)
            {
    			for (int j = -1; j <= 1; j++) 
                {
    				// 如果是自己，则跳过
    				if (i == 0 && j == 0)
    					continue;
                    
    				int x = node.X + i;
    				int y = node.Y + j;
    				// 判断是否越界，如果没有，加到列表中
    				if (x < rows && x >= 0 && y < columns && y >= 0)
    					list.Add (NodeArray [x, y]);
    			}
    		}
    		return list;
    	}

    	// 更新路径
    	public void UpdatePath(List<Node> lines) 
        {
            int curListSize = pathNodeList.Count;

            for (int i = 0; i < lines.Count; i++) 
            {
                if (i < curListSize)
                    pathNodeList[i].localScale = Vector3.one;
                else 
                {
                    var node = Instantiate (pathLine).rectTransform;
//                    node.sizeDelta = Vector2.one * size;
                    node.SetParent (pathLineRoot, false);
                    pathNodeList.Add (node);
    			}

                pathNodeList[i].anchoredPosition = lines[i].Position;
    		}

    		for (int i = lines.Count; i < curListSize; i++) 
                pathNodeList[i].localScale = Vector3.zero;
    		
    	}
    }
}
