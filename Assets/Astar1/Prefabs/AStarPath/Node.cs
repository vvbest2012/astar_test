﻿using UnityEngine;
using System.Collections;

namespace Astar1
{
    public class Node : IHeapItem<Node>
    {
        int heapIndex;

        public bool isWall;
        public int gCost;
        public int hCost;  
        public Node parent;

        public int X { get; private set; }
        public int Y { get; private set; }
        public int FCost { get {return gCost + hCost; } }
        public Vector2 Position { get; private set; }

        public Node(int x, int y, int size) {
            X = x;
            Y = y;

            float center = size * 0.5f;
            Position = new Vector2(x * size + center, y * size + center);
            //                Debug.Log(Position);
        }

        #region IComparable implementation

        public int CompareTo (Node other)
        {
            int compare = FCost.CompareTo(other.FCost);
            if (compare == 0) {
                compare = hCost.CompareTo(other.hCost);
            }

            return -compare;
        }

        #endregion

        #region IHeapItem implementation

        public int HeapIndex
        {
            get {
                return heapIndex;
            }
            set {
                heapIndex = value;
            }
        }

        #endregion
    }
}
