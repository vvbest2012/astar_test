﻿using System.Collections;

namespace Astar2
{
    public class NodeSort
    {
        private ArrayList nodes = new ArrayList();

        public int Length { get { return this.nodes.Count; } }

        public bool Contains (object node)
        {    
            for (int cnt = 0; cnt < nodes.Count; cnt++)
            {
                if (((Node)node).position == ((Node)nodes[cnt]).position) return true;
            }
            return false;
        }

        public Node First ()
        {
            if (this.nodes.Count > 0)
            {
                return (Node)this.nodes[0];
            }
            return null;
        }

        public void Push (Node node)
        {

            this.nodes.Add(node);
            this.nodes.Sort();
        }

        public void Remove (Node node)
        {    
            this.nodes.Remove(node);
            this.nodes.Sort();
        }
    }
}