﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Astar2
{
    public class AStar
    {
        public static NodeSort closedList, openList;

        private static float HeuristicEstimateCost (Node curNode, Node goalNode)
        {
            Vector3 vectorCost = goalNode.position - curNode.position;

            return vectorCost.magnitude;    
        }

        public static ArrayList FindPath (Node start, Node goal)
        {        
            openList = new NodeSort();
            openList.Push(start);
            start.G_Cost = 0.0f;
            start.H_Cost = HeuristicEstimateCost(start, goal);

            closedList = new NodeSort();
            Node node = null;

            while (openList.Length != 0)
            {    
                node = openList.First();

                //Push the current node to the closed list
                closedList.Push(node);
                //and remove it from openList
                openList.Remove(node);

                //Check if the current node is the goal node
                if (node.position == goal.position)
                {
                    return CalculatePath(node);
                }

                //Create an ArrayList to store the neighboring nodes
                ArrayList neighbours = new ArrayList();
                NodeManager.instance.GetNeighbours(node, neighbours);

                Node neighbourNode;

                for (int i = 0; i < neighbours.Count; i++)
                {
                    neighbourNode = (Node)neighbours[i];
                    if (!closedList.Contains(neighbourNode))
                    {
                        float cost;
                        float totalCost;
                        float neighbourNodeEstCost;
                        if (!openList.Contains(neighbourNode))
                        { 
                            cost = HeuristicEstimateCost(node, neighbourNode); 
                            totalCost = node.G_Cost + cost; 
                            neighbourNodeEstCost = HeuristicEstimateCost(neighbourNode, goal); 
                            neighbourNode.G_Cost = totalCost; 
                            neighbourNode.parent = node; 
                            neighbourNode.H_Cost = neighbourNodeEstCost;
                    
                            openList.Push(neighbourNode); 
                        }
                        else
                        {
                            cost = HeuristicEstimateCost(node, neighbourNode); 
                            totalCost = node.G_Cost + cost; 

                            if (neighbourNode.G_Cost > totalCost)
                            {

                                neighbourNode.G_Cost = totalCost;
                                neighbourNode.parent = node;                            
                            }                        
                        }                    
                    }                
                }            
            }

            if (node.position != goal.position)
            {            
                Debug.LogError("Goal Not Found");
                return null;            
            }

            return CalculatePath(node);        
        }

        private static ArrayList CalculatePath (Node node)
        {       
            ArrayList list = new ArrayList();

            while (node != null)
            {           
                list.Add(node);
                node = node.parent;         
            }

            list.Reverse();

            foreach (var item in list)
            {
                TestCube(((Node)item).position);
            }

//            return list;
//            return LineOfSight(list);
            return SimplifyPath(list);
        }

        static GameObject TestCube (Vector3 pos)
        {
            var cube = GameObject.CreatePrimitive(PrimitiveType.Cube);
            cube.transform.localScale = Vector3.one * 0.1f;
            cube.transform.position = pos;
            cube.GetComponent<BoxCollider>().enabled = false;

            return cube;
        }

        static ArrayList SimplifyPath(ArrayList path) {
            ArrayList waypoints = new ArrayList();
            Vector3 directionOld = Vector3.zero;

            Node startNode = (Node)path[0];
            Node nextNode;

            int checkIndex = 1;

            while (checkIndex < path.Count)
            {
                nextNode = (Node)path[checkIndex];
                Vector3 directionNew = nextNode.position - startNode.position;

                if (directionNew != directionOld) {
                    waypoints.Add(startNode);
                }

                Vector3 dir = nextNode.position - startNode.position;
                float angle = Vector3.Angle(dir, Vector3.forward);
                float dot = Vector3.Dot(Vector3.right, dir);

                Debug.Log(angle + " / " + dot);

                startNode = nextNode;
                directionOld = directionNew;
                checkIndex++;
            }

            waypoints.Add((Node)path[path.Count - 1]);

            foreach (var item in waypoints)
            {
                NodeManager.instance.tempList.Add(((Node)item).position);
            }

          

            return waypoints;
        }



        private static ArrayList LineOfSight (ArrayList path)
        {
            ArrayList list = new ArrayList();
            list.Add((Node)path[0]);

            Node startNode = (Node)path[0];
            Node nextNode;

            int checkIndex = 1;

//            GameObject cube = TestCube(startNode.position);
//            cube.name = "0";

            while (checkIndex < path.Count)
            {
                nextNode = ((Node)path[checkIndex]);
//                cube = TestCube(nextNode.position);
//                cube.name = checkIndex + "";
                if (!Physics.Linecast(startNode.position, nextNode.position))
                {
//                    cube.name += " !Physics.Linecast";
                    checkIndex++;
                }
                else
                {
                    if (checkIndex == path.Count - 1)
                    {
//                        cube.name += " aaa";
//                        list.Add((Node)path[checkIndex - 1]);
                        list.Add((Node)path[checkIndex]);
                    }
                    else
                    {
//                        cube.name += " bbb";
                        list.Add((Node)path[checkIndex - 1]);
                        startNode = (Node)path[checkIndex - 1];
//                    nextNode = (Node)path[checkIndex];
                    }

                    checkIndex++;
                }
            }

            foreach (var item in list)
            {
                Node node = (Node)item;
                NodeManager.instance.tempList.Add(node.position);
            }

            return list;
        }
    }
}