﻿using UnityEngine;
using System.Collections;

public class NewBehaviourScript1 : MonoBehaviour {

    public Transform aTest;
    public Transform bTest;
    public Transform arrow;

    Vector3 rot;


	// Use this for initialization
	void Start () {
        Input.compass.enabled = true;
	}
	
	// Update is called once per frame
	void Update () {

//        rot.z = Input.compass.trueHeading;
//        aTest.localEulerAngles = rot;

        Vector3 dir = bTest.position - aTest.position;
        float angle = Vector3.Angle(dir, aTest.up);
        float dot = Vector3.Dot(aTest.right, dir);

        rot.z = dot > 0 ? angle : -angle;
        arrow.localEulerAngles = rot;

        Debug.Log(angle + " / " + dot);
	}
}
