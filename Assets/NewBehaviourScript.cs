﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using Astar1;

public class NewBehaviourScript : MonoBehaviour {

    [SerializeField]
    int gridRows;
    [SerializeField]
    int gridColumns;
    [SerializeField]
    int gridSize;
    [SerializeField]
    Vector2 startCoordinate;
    [SerializeField]
    Vector2 endCoordinate;
    [SerializeField]
    RectTransform startImg;
    [SerializeField]
    RectTransform endImg;
    [SerializeField]
    Grid grid;
    [SerializeField]
    FindPath find;

	// Use this for initialization
	void Start () {
        grid.Init(gridRows, gridColumns, gridSize);

       

	}
	
	// Update is called once per frame
	void Update () {
        startImg.anchoredPosition = grid.GetNode(startCoordinate).Position;
        endImg.anchoredPosition = grid.GetNode(endCoordinate).Position;

        find.FindingPath(startCoordinate, endCoordinate);

       
	}
}
